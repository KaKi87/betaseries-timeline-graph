export default ({
    extension,
    responseType
}) => new Promise((resolve, reject) => {
    const inputElement = document.createElement('input');
    inputElement.setAttribute('type', 'file');
    inputElement.setAttribute('accept', `.${extension}`);
    inputElement.addEventListener('change', event => {
        if(responseType){
            const fileReader = new FileReader();
            fileReader.addEventListener('load', event => resolve(event.target.result));
            fileReader.addEventListener('error', reject);
            fileReader[{
                'text': 'readAsText',
                'arraybuffer': 'readAsArrayBuffer'
            }[responseType]](event.target.files[0]);
        }
        else resolve(event.target.files[0]);
    });
    inputElement.click();
});