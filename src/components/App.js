import AppHeader from './AppHeader.js';
import AppMain from './AppMain.js';

import style from './App.css' assert { type: 'css' };

export default {
    components: {
        AppHeader,
        AppMain
    },
    'data': () => ({
        style
    }),
    template: `
        <AppHeader>
        </AppHeader>
        <AppMain>
        </AppMain>
    `
};