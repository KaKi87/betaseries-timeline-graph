import style from './_link.css' assert { type: 'css' };

export default {
    props: {
        url: undefined
    },
    'data': () => ({
        style
    }),
    template: `
        <a
            class="link"
            :href="url"
            target="_blank"
        ><slot></slot></a>
    `
};