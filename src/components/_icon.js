import style from './_icon.css' assert { type: 'css' };

export default {
    props: {
        url: undefined
    },
    'data': () => ({
        style
    }),
    template: `
        <span
            class="icon"
            :style="{
                '--mask-image': 'url(' + url + ')'
            }"
        ></span>
    `
};