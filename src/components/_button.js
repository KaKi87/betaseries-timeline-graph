import style from './_button.css' assert { type: 'css' };

export default {
    props: {
        isEnabled: {
            default: true
        }
    },
    'data': () => ({
        style
    }),
    template: `
        <button
            class="button"
            :disabled="!isEnabled"
        >
            <slot></slot>
        </button>
    `
};