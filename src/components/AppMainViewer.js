import githubContributionGraph from './_githubContributionGraph.js';
import icon from './_icon.js';

import style from './AppMainViewer.css' assert { type: 'css' };

export default {
    components: {
        githubContributionGraph,
        icon
    },
    props: {
        episodes: undefined,
        films: undefined
    },
    'data': () => ({
        style,
        selectedEpisodesAndFilms: undefined
    }),
    computed: {
        episodesAndFilms: function(){
            return [
                ...this.episodes.map(({
                    id,
                    show,
                    episode,
                    timestamp
                }) => ({
                    type: 'episode',
                    id,
                    title: `${show} ${episode}`,
                    timestamp
                })),
                ...this.films.map(({
                    id,
                    title,
                    timestamp
                }) => ({
                    type: 'film',
                    id,
                    title,
                    timestamp
                }))
            ];
        },
        timestamps: function(){
            return this.episodesAndFilms.map(({ timestamp }) => timestamp).sort((a, b) => b - a);
        },
        timestampsByYear: function(){
            const timestampsByYear = [];
            let year, timestamps;
            for(const timestamp of this.timestamps){
                const _year = this.getYear(timestamp);
                if(year && year === _year && timestamps)
                    timestamps.push(timestamp);
                else {
                    timestamps = [timestamp];
                    year = _year;
                    timestampsByYear.push(timestamps);
                }
            }
            return timestampsByYear;
        },
        'selectedDate': function(){
            return this.selectedEpisodesAndFilms && this.selectedEpisodesAndFilms.length ? this.getDate(this.selectedEpisodesAndFilms[0].timestamp) : undefined;
        }
    },
    methods: {
        getYear: function(timestamp){
            return new Date(timestamp).getFullYear();
        },
        onDayClicked: function(timestamps){
            this.selectedEpisodesAndFilms = this.episodesAndFilms.filter(({ timestamp }) => timestamps.includes(timestamp));
        },
        getDate: function(timestamp){
            return new Date(timestamp).toLocaleDateString();
        },
        onEpisodeOrFilmClicked: function({ id, type }){
            window.open(`https://www.betaseries.com/${type}/${id}`);
        }
    },
    template: `
        <section class="App__Main__Viewer">
            <div class="App__Main__Viewer__YearsContainer">
                <ul class="App__Main__Viewer__YearsContainer__Years">
                    <li
                        v-for="timestamps in timestampsByYear"
                        class="App__Main__Viewer__YearsContainer__Years__Year"
                    >
                        <h3 class="App__Main__Viewer__YearsContainer__Years__Year__Heading">{{ getYear(timestamps[0]) }}</h3>
                        <githubContributionGraph
                            :timestamps="timestamps"
                            @day-clicked="onDayClicked"
                        ></githubContributionGraph>
                    </li>
                </ul>
            </div>
            <div class="App__Main__Viewer__DetailsContainer">
                <div
                    v-if="selectedDate"
                    class="App__Main__Viewer__DetailsContainer__Details"
                >
                    <h2 class="App__Main__Viewer__DetailsContainer__Details__Heading">{{ selectedDate }}</h2>
                    <table class="App__Main__Viewer__DetailsContainer__Details__Table">
                        <thead>
                            <th>Type</th>
                            <th>Title</th>
                            <th>Date</th>
                        </thead>
                        <tbody>
                            <tr
                                v-for="episodeOrFilm in selectedEpisodesAndFilms"
                                @click="onEpisodeOrFilmClicked(episodeOrFilm)"
                            >
                                <td>
                                    <icon
                                        class="App__Main__Viewer__DetailsContainer__Details__Table__Icon"
                                        :url="{
                                            'episode': 'https://uxwing.com/wp-content/themes/uxwing/download/computers-mobile-hardware/computer-laptop-icon.svg',
                                            'film': 'https://icons8.com/vue-static/landings/animated-icons/icons/movie/movie.svg'
                                        }[episodeOrFilm.type]"
                                    ></icon>
                                </td>
                                <td>{{ episodeOrFilm.title }}</td>
                                <td>{{ getDate(episodeOrFilm.timestamp) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    `
};