import csv from 'https://cdn.jsdelivr.net/npm/csvtojson@2.0.10/+esm';

import openFile from '../../lib/openFile.js';

import link from './_link.js';
import icon from './_icon.js';
import button from './_button.js';

import style from './AppMainImport.css' assert { type: 'css' };

export default {
    components: {
        'c-link': link,
        icon,
        'c-button': button
    },
    props: {
        episodes: undefined,
        films: undefined
    },
    'data': () => ({
        style
    }),
    computed: {
        importedEpisodeCount: function(){
            return this.episodes.length;
        },
        importedFilmCount: function(){
            return this.films.length;
        }
    },
    methods: {
        importEpisodes: async function(){
            this.episodes.splice(
                0,
                this.episodes.length,
                ...(await csv().fromString(await openFile({
                    extension: 'csv',
                    responseType: 'text'
                }))).map(({
                    id,
                    show,
                    episode,
                    title,
                    date
                }) => ({
                    id,
                    show,
                    episode,
                    title,
                    timestamp: new Date(date).valueOf()
                }))
            );
        },
        importFilms: async function(){
            this.films.splice(
                0,
                this.films.length,
                ...(await csv().fromString(await openFile({
                    extension: 'csv',
                    responseType: 'text'
                }))).map(({
                    id,
                    title,
                    date
                }) => date ? {
                    id,
                    title,
                    timestamp: new Date(date).valueOf()
                } : undefined).filter(Boolean)
            );
        },
        onDone: function(){
            this.$emit('done');
        }
    },
    template: `
        <section class="App__Main__Import">
            <div class="App__Main__Import__Inner">
                <h2 class="App__Main__Import__Inner__Heading">Import files from your <c-link url="https://www.betaseries.com/compte/avance">account settings</c-link></h2>
                <button
                    class="App__Main__Import__Inner__FileButton App__Main__Import__Inner__FileButton--episodes"
                    @click="importEpisodes"
                >
                    episodes.csv
                    <icon
                        class="App__Main__Import__Inner__FileButton__Icon"
                        url="https://uxwing.com/wp-content/themes/uxwing/download/computers-mobile-hardware/computer-laptop-icon.svg"
                    ></icon>
                    {{ importedEpisodeCount }} episode(s) imported
                </button>
                <button
                    class="App__Main__Import__Inner__FileButton App__Main__Import__Inner__FileButton--films"
                    @click="importFilms"
                >
                    films.csv
                    <icon
                        class="App__Main__Import__Inner__FileButton__Icon"
                        url="https://icons8.com/vue-static/landings/animated-icons/icons/movie/movie.svg"
                    ></icon>
                    {{ importedFilmCount }} film(s) imported
                </button>
                <c-button
                    class="App__Main__Import__Inner__ContinueButton"
                    :is-enabled="importedEpisodeCount || importedFilmCount"
                    @click="onDone"
                >Done</c-button>
            </div>           
        </section>
    `
};