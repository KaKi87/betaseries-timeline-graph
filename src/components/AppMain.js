import AppMainImport from './AppMainImport.js';
import AppMainViewer from './AppMainViewer.js';

import style from './AppMain.css' assert { type: 'css' };

export default {
    components: {
        AppMainImport,
        AppMainViewer
    },
    'data': () => ({
        style,
        activeSection: 'import',
        episodes: [],
        films: []
    }),
    methods: {
        onImportDone: function(){
            this.activeSection = 'viewer';
        }
    },
    template: `
        <main class="App__Main">
            <AppMainImport
                v-if="activeSection === 'import'"
                :episodes="episodes"
                :films="films"
                @done="onImportDone"
            ></AppMainImport>
            <AppMainViewer
                v-if="activeSection === 'viewer'"
                :episodes="episodes"
                :films="films"
            ></AppMainViewer>
        </main>
    `
};