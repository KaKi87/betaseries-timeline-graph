import link from './_link.js';

import style from './AppHeader.css' assert { type: 'css' };

export default {
    components: {
        'c-link': link
    },
    'data': () => ({
        style
    }),
    template: `
        <header class="App__Header">
            <h1>BetaSeries timeline graph</h1>
            <c-link url="https://git.kaki87.net/KaKi87/betaseries-timeline-graph">Source code</c-link>
        </header>
    `
};