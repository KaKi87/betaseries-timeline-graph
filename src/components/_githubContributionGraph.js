import { create as createGitHubContributionGraph } from 'https://cdn.jsdelivr.net/gh/KaKi87-2/github-contribution-graph@133f74f5560d93ea02568447ffdac06b03dc38de/main.js';

export default {
    props: {
        timestamps: undefined
    },
    mounted: function(){
        this.$el.appendChild(
            createGitHubContributionGraph({
                timestamps: this.timestamps,
                onClick: ({ timestamps }) => this.$emit('day-clicked', timestamps)
            })
        );
    },
    template: `
        <div class="githubContributionGraph"></div>
    `
};