import App from './components/App.js';

export const createMyApp = ({ createApp }) => {
    const app = createApp(App);

    app.mixin({
        'mounted': function(){
            if(this.style && !document.adoptedStyleSheets.includes(this.style))
                document.adoptedStyleSheets.push(this.style);
        }
    });

    return app;
};